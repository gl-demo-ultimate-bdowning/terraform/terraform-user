# PROJECT NAME TAG
variable "project_name" {
  description = "Name of the Project"
  default     = "terraform-compliance"
  type        = string
  sensitive   = false
}
