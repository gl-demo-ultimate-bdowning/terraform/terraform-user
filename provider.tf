#terraform {
#  required_providers {
#    aws = {
#      source  = "hashicorp/aws"
#      version = "5.31.0"
#    }
#  }
#  backend "s3" {
#    bucket  = "tf-compliance-test"
#    key     = "compliance-demo/terraform-state"
#    region  = "us-east-1"
#    encrypt = "true"
#  }
#}
#provider "aws" {
#  region = var.region
#}

terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "5.10.0"
    }
  }
}

provider "google" {
 # credentials = file("${GCP_SVC_ACCOUNT}")
  project = "pandora-gcp-303417"
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_compute_network" "vpc_network" {
  name = "terraform-network"
}
